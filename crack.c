#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "md5.h"

const int PASS_LEN = 20; // Maximum any password will be
const int HASH_LEN = 33; // Length of MD5 hash strings

FILE * checkOpen(char * filename, char * mode) {
    FILE * f = fopen(filename, mode);
    if (!f) {
        char err[64];
        sprintf(err, "File '%s' cannot be opened", filename);
        perror(err);
        exit(1);
    }
    return f;
}

// Given a target hash, crack it. Return the matching
// password.
char * crackHash(char * targetHash, char * dictionaryFilename) {
    // Open the dictionary file
    FILE * dictionary = checkOpen(dictionaryFilename, "r");

    // Loop through the dictionary file, one line
    // at a time.
    static char line[64];
    while (fgets(line, 64, dictionary)) {
        // get rid of trailing '\n'
        if (line[strlen(line) - 1] == '\n')
            line[strlen(line) - 1] = '\0';

        // Hash each password. Compare to the target hash.
        char * checkHash = md5(line, strlen(line));

        // If they match, return the corresponding password.
        if (strcmp(checkHash, targetHash) == 0) {
            free(checkHash);

            fclose(dictionary);
            return line;
        }

        // Free up memory
        free(checkHash);
    }

    // Password wasn't found in dictionary
    fclose(dictionary);
    return NULL;
}

int main(int argc, char * argv[]) {
    if (argc < 3) {
        fprintf(stderr, "Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    char * dictionaryFilename = argv[2];
    // Open the hash file for reading.
    FILE * hashFile = checkOpen(argv[1], "r");

    // For each hash, crack it by passing it to crackHash
    char hash[64];
    while (fgets(hash, 64, hashFile)) {
        // get rid of trailing '\n'
        if (hash[strlen(hash) - 1] == '\n')
            hash[strlen(hash) - 1] = '\0';

        char * crackedPassword = crackHash(hash, dictionaryFilename);

        // Display the hash along with the cracked password:
        // 5d41402abc4b2a76b9719d911017c592 hello
        printf("%s %s\n", hash, crackedPassword);

    }

    // Close the hash file
    fclose(hashFile);

    return (0);
}